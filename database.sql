-- CREATE DATABASE poker_tools WITH OWNER postgres ENCODING 'UTF8' TABLESPACE = pg_default TEMPLATE template0;
-- CREATE USER pokertools WITH PASSWORD 'pokertools' SUPERUSER;
-- DROP SCHEMA IF EXISTS poker_tools CASCADE;
-- CREATE SCHEMA poker_tools;

CREATE TABLE poker_tools.user (
    id SERIAL NOT NULL,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(64) NOT NULL,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    email VARCHAR(500) NOT NULL
);

ALTER TABLE poker_tools.user ADD CONSTRAINT user_pkey PRIMARY KEY (id);
ALTER TABLE poker_tools.user ADD CONSTRAINT user_username_ukey UNIQUE (username);

INSERT INTO poker_tools.user (username, password, first_name, last_name, email) VALUES ('admin', '$2a$10$aB3WExg2FqmDPrH0CgCJuOW.OJasB/6jG2kXuh46QzbCo9unuJtHG', null, null, 'eager.code@gmail.com');

CREATE TABLE poker_tools.blinds (
    id SERIAL NOT NULL,
    caption VARCHAR(500) NOT NULL,
    deleted BOOLEAN NOT NULL DEFAULT false,
    user_id INTEGER NOT NULL REFERENCES poker_tools.user(id)
);

ALTER TABLE poker_tools.blinds ADD CONSTRAINT blinds_pkey PRIMARY KEY (id);

CREATE TABLE poker_tools.bet (
    id SERIAL NOT NULL,
    blinds_id INTEGER NOT NULL REFERENCES poker_tools.blinds(id),
    level INTEGER NOT NULL,
    big_blind NUMERIC(8, 2) NOT NULL,
    small_blind NUMERIC(8, 2) NOT NULL,
    ante NUMERIC(8, 2)
);

ALTER TABLE poker_tools.bet ADD CONSTRAINT bet_pkey PRIMARY KEY (id);

CREATE TABLE poker_tools.poker_chips_set (
    id SERIAL NOT NULL,
    caption VARCHAR(500) NOT NULL,
    deleted BOOLEAN NOT NULL DEFAULT false,
    user_id INTEGER NOT NULL REFERENCES poker_tools.user(id)
);

ALTER TABLE poker_tools.poker_chips_set ADD CONSTRAINT poker_chips_set_pkey PRIMARY KEY (id);

CREATE TABLE poker_tools.chip (
    id SERIAL NOT NULL,
    poker_chips_set_id INTEGER NOT NULL REFERENCES poker_tools.poker_chips_set(id),
    color VARCHAR(10) NOT NULL CHECK (color IN ('BLACK', 'BLUE', 'BROWN', 'GREEN', 'GREY', 'ORANGE', 'PINK', 'PURPLE', 'RED', 'WHITE', 'YELLOW')),
    value NUMERIC(8, 2) NOT NULL
);

ALTER TABLE poker_tools.chip ADD CONSTRAINT chip_pkey PRIMARY KEY (id);

CREATE TABLE poker_tools.prize_pool (
    id SERIAL NOT NULL,
    caption VARCHAR(500) NOT NULL,
    deleted BOOLEAN NOT NULL DEFAULT false,
    user_id INTEGER NOT NULL REFERENCES poker_tools.user(id)
);

ALTER TABLE poker_tools.prize_pool ADD CONSTRAINT prize_pool_pkey PRIMARY KEY (id);

CREATE TABLE poker_tools.prize (
    id SERIAL NOT NULL,
    prize_pool_id INTEGER NOT NULL REFERENCES poker_tools.prize_pool(id),
    place_from INTEGER NOT NULL,
    place_until INTEGER NOT NULL,
    amount NUMERIC(8, 2) NOT NULL
);

ALTER TABLE poker_tools.prize ADD CONSTRAINT prize_pkey PRIMARY KEY (id);

CREATE TABLE poker_tools.participant (
    id SERIAL NOT NULL,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    nickname VARCHAR(50),
    deleted BOOLEAN NOT NULL DEFAULT false,
    user_id INTEGER NOT NULL REFERENCES poker_tools.user(id)
);

ALTER TABLE poker_tools.participant ADD CONSTRAINT participant_pkey PRIMARY KEY (id);

CREATE TABLE poker_tools.tournament (
    id SERIAL NOT NULL,
    caption VARCHAR(500) NOT NULL,
    password VARCHAR(50),
    start_date TIMESTAMP,
    finish_date TIMESTAMP,
    level_time INTEGER NOT NULL,
    buy_in NUMERIC(8, 2),
    re_buy NUMERIC(8, 2),
    add_on NUMERIC(8, 2),
    break_minute INTEGER,
    break_length INTEGER,
    poker_chips_set_id INTEGER REFERENCES poker_tools.poker_chips_set(id),
    blinds_id INTEGER NOT NULL REFERENCES poker_tools.blinds(id),
    prize_pool_id INTEGER REFERENCES poker_tools.prize_pool(id),
    deleted BOOLEAN NOT NULL DEFAULT false,
    user_id INTEGER NOT NULL REFERENCES poker_tools.user(id)
);

ALTER TABLE poker_tools.tournament ADD CONSTRAINT tournament_pkey PRIMARY KEY (id);

CREATE TABLE poker_tools.tournament_participant (
    tournament_id INTEGER NOT NULL REFERENCES poker_tools.tournament(id),
    participant_id INTEGER NOT NULL REFERENCES poker_tools.participant(id)
);

ALTER TABLE poker_tools.tournament_participant ADD CONSTRAINT tournament_participant_pkey PRIMARY KEY (tournament_id, participant_id);

ALTER USER pokertools SET search_path to 'poker_tools';

CREATE TABLE poker_tools.tournament_action (
    id SERIAL NOT NULL,
    tournament_id INTEGER NOT NULL REFERENCES poker_tools.tournament(id),
    user_id INTEGER REFERENCES poker_tools.user(id),
    action VARCHAR(50) NOT NULL CHECK (action IN ('START', 'FINISH', 'PAUSE', 'RESUME', 'RESTART_FROM_LEVEL')),
    details VARCHAR(50),
    date TIMESTAMP
);

ALTER TABLE poker_tools.tournament_action ADD CONSTRAINT tournament_action_pkey PRIMARY KEY (id);

INSERT INTO poker_tools.tournament_action SELECT nextval('poker_tools.tournament_action_id_seq'), id, user_id, 'START', start_date FROM poker_tools.tournament t where t.start_date IS NOT NULL;
INSERT INTO poker_tools.tournament_action SELECT nextval('poker_tools.tournament_action_id_seq'), id, user_id, 'FINISH', finish_date FROM poker_tools.tournament t where t.finish_date IS NOT NULL;

ALTER TABLE poker_tools.tournament DROP COLUMN start_date;
ALTER TABLE poker_tools.tournament DROP COLUMN finish_date;
ALTER TABLE poker_tools.tournament ADD COLUMN version INTEGER NOT NULL DEFAULT 1;

CREATE INDEX blinds_user_id_idx ON poker_tools.blinds(user_id);
CREATE INDEX bet_blinds_id_idx ON poker_tools.bet(blinds_id);
CREATE INDEX poker_chips_set_user_id_idx ON poker_tools.poker_chips_set(user_id);
CREATE INDEX chip_poker_chips_set_id_idx ON poker_tools.chip(poker_chips_set_id);
CREATE INDEX prize_pool_user_id_idx ON poker_tools.prize_pool(user_id);
CREATE INDEX prize_prize_pool_id_idx ON poker_tools.prize(prize_pool_id);
CREATE INDEX participant_user_id_idx ON poker_tools.participant(user_id);
CREATE INDEX tournament_poker_chips_set_id_idx ON poker_tools.tournament(poker_chips_set_id);
CREATE INDEX tournament_blinds_id_idx ON poker_tools.tournament(blinds_id);
CREATE INDEX tournament_prize_pool_id_idx ON poker_tools.tournament(prize_pool_id);
CREATE INDEX tournament_user_id_idx ON poker_tools.tournament(user_id);
CREATE INDEX tournament_action_tournament_id_idx ON poker_tools.tournament_action(tournament_id);
CREATE INDEX tournament_action_user_id_idx ON poker_tools.tournament_action(user_id);
CREATE INDEX tournament_action_action_idx ON poker_tools.tournament_action(action);
CREATE INDEX tournament_action_date_idx ON poker_tools.tournament_action(date);

ALTER TABLE poker_tools.tournament ADD COLUMN add_on_level INTEGER;
ALTER TABLE poker_tools.tournament ADD COLUMN chips_at_beginning INTEGER;
ALTER TABLE poker_tools.tournament ADD COLUMN chips_for_re_buy INTEGER;
ALTER TABLE poker_tools.tournament ADD COLUMN chips_for_add_on INTEGER;

ALTER TABLE poker_tools.tournament_action DROP CONSTRAINT tournament_action_action_check;
ALTER TABLE poker_tools.tournament_action ADD CONSTRAINT tournament_action_action_check CHECK (action IN ('START', 'FINISH', 'LEVEL_UP', 'PAUSE', 'RESUME', 'RESTART_FROM_LEVEL', 'PARTICIPANT_DROP', 'PARTICIPANT_REBUY', 'PARTICIPANT_ADDON'));

ALTER TABLE poker_tools.prize_pool ADD COLUMN type VARCHAR(12) NOT NULL DEFAULT 'CURRENCY' CHECK (type IN ('CURRENCY', 'PERCENTAGES'));
ALTER TABLE poker_tools.prize_pool ADD COLUMN rake NUMERIC(8, 2);

CREATE TABLE poker_tools.pause (
    id SERIAL NOT NULL,
    caption VARCHAR(500) NOT NULL,
    pause_minute INTEGER,
    number_of_levels INTEGER,
    pause_length INTEGER NOT NULL,
    add_on_pause_length INTEGER,
    deleted BOOLEAN NOT NULL DEFAULT false,
    user_id INTEGER NOT NULL REFERENCES poker_tools.user(id)
);

ALTER TABLE poker_tools.pause ADD CONSTRAINT pause_pkey PRIMARY KEY (id);

ALTER TABLE poker_tools.tournament ADD COLUMN pause_id INTEGER REFERENCES poker_tools.pause(id);

ALTER TABLE poker_tools.pause ADD COLUMN temp_tournament_id INTEGER;
INSERT INTO poker_tools.pause(caption, pause_minute, number_of_levels, pause_length, add_on_pause_length, deleted, user_id, temp_tournament_id) SELECT 'Pause for tournament ' || caption, break_minute, null, break_length, null, false, user_id, id FROM poker_tools.tournament WHERE break_minute IS NOT NULL;
UPDATE poker_tools.tournament t SET pause_id = (SELECT id FROM poker_tools.pause WHERE temp_tournament_id = t.id) WHERE t.break_minute IS NOT NULL;
ALTER TABLE poker_tools.pause DROP COLUMN temp_tournament_id;

ALTER TABLE poker_tools.tournament DROP COLUMN break_minute;
ALTER TABLE poker_tools.tournament DROP COLUMN break_length;

ALTER TABLE poker_tools.tournament_action ALTER COLUMN date SET NOT NULL;