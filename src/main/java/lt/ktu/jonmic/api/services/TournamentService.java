package lt.ktu.jonmic.api.services;

import lt.ktu.jonmic.api.constants.Action;
import lt.ktu.jonmic.api.dao.TournamentRepository;
import lt.ktu.jonmic.api.entities.Participant;
import lt.ktu.jonmic.api.entities.Tournament;
import lt.ktu.jonmic.api.entities.TournamentAction;
import lt.ktu.jonmic.api.entities.User;
import lt.ktu.jonmic.api.utils.DateUtils;
import lt.ktu.jonmic.api.utils.TournamentUtils;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TournamentService {

    private static final int TOURNAMENT_EXPIRE_LENGTH_IN_DAYS = 30;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private TournamentRepository tournamentRepository;

    @Transactional
    public Tournament save(Tournament tournament) {
        tournament.setVersion(tournament.getVersion() != null ? tournament.getVersion() + 1 : 1);
        Tournament result = tournamentRepository.save(tournament);
        simpMessagingTemplate.convertAndSend("/topic/tournament/" + tournament.getId(), tournament);
        return result;
    }

    @Transactional
    public void saveTournamentAction(Tournament tournament, Action action) {
        saveTournamentAction(tournament, action, null, null);
    }

    @Transactional
    public void saveTournamentAction(Tournament tournament, Action action, String details) {
        saveTournamentAction(tournament, action, details, null);
    }

    @Transactional
    public void saveTournamentAction(Tournament tournament, Action action, String details, User user) {
        TournamentAction tournamentAction = new TournamentAction();
        tournamentAction.setAction(action);
        tournamentAction.setDetails(details);
        tournamentAction.setDate(new Date());
        tournamentAction.setUser(user);
        tournament.getTournamentActions().add(tournamentAction);
        save(tournament);
    }

    @Transactional
    public void delete(Tournament tournament) {
        tournament.setDeleted(true);
        save(tournament);
    }

    @Transactional
    public void levelUpTournamentIfNeeded(Tournament tournament, Date now) {
        int currentLevel = TournamentUtils.getCurrentTournamentLevel(tournament);
        Date nextLevelStartTime = TournamentUtils.getNextLevelStartTime(tournament, currentLevel, now);
        if (nextLevelStartTime.before(now)) {
            saveTournamentAction(tournament, Action.LEVEL_UP, String.valueOf(currentLevel + 1));
        }
    }

    @Transactional
    public void pauseTournamentIfNeeded(Tournament tournament, Date now) {
        if (tournament.getPause() != null && !TournamentUtils.isTournamentPaused(tournament)) {
            if (TournamentUtils.shouldTournamentBePaused(tournament, now)) {
                saveTournamentAction(tournament, Action.PAUSE);
            }
        }
    }

    @Transactional
    public void resumeTournamentIfNeeded(Tournament tournament, Date now) {
        if (tournament.getPause() != null && TournamentUtils.isTournamentPaused(tournament)) {
            if (!TournamentUtils.shouldTournamentBePaused(tournament, now)) {
                if (TournamentUtils.getLastTournamentAction(tournament, Action.PAUSE).getUser() == null) {
                    saveTournamentAction(tournament, Action.RESUME);
                }
            }
        }
    }

    @Transactional
    public void stopTournamentIfItIsExpired(Tournament tournament, Date now) {
        Date tournamentStartDate = TournamentUtils.getTournamentStartDate(tournament);
        Date tournamentExpireDate = DateUtils.addDays(tournamentStartDate, TOURNAMENT_EXPIRE_LENGTH_IN_DAYS);
        if (tournamentExpireDate.before(now)) {
            saveTournamentAction(tournament, Action.FINISH);
        }
    }

    @Transactional
    public void stopTournamentIfAllParticipantsAreDropped(Tournament tournament) {
        if (tournament.getParticipants().isEmpty()) {
            return;
        }

        for (Participant participant : tournament.getParticipants()) {
            if (TournamentUtils.isParticipantStillPlaying(tournament, participant)) {
                return;
            }
        }

        saveTournamentAction(tournament, Action.FINISH);
    }

}
