package lt.ktu.jonmic.api.services;

import lt.ktu.jonmic.api.dao.PrizePoolRepository;
import lt.ktu.jonmic.api.dao.TournamentRepository;
import lt.ktu.jonmic.api.entities.PrizePool;
import lt.ktu.jonmic.api.entities.Tournament;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PrizePoolService {

    @Autowired
    private PrizePoolRepository prizePoolRepository;

    @Autowired
    private TournamentRepository tournamentRepository;

    @Autowired
    private TournamentService tournamentService;

    @Transactional
    public PrizePool save(PrizePool prizePool) {
        if (!prizePool.isNew()) {
            List<Tournament> tournaments = tournamentRepository.findByPrizePoolId(prizePool.getId());
            for (Tournament tournament : tournaments) {
                tournamentService.save(tournament);
            }
        }

        return prizePoolRepository.save(prizePool);
    }

    @Transactional
    public void delete(PrizePool prizePool) {
        prizePool.setDeleted(true);
        prizePoolRepository.save(prizePool);
    }

}
