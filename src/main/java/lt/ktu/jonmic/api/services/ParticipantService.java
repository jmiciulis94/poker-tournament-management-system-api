package lt.ktu.jonmic.api.services;

import lt.ktu.jonmic.api.dao.ParticipantRepository;
import lt.ktu.jonmic.api.dao.TournamentRepository;
import lt.ktu.jonmic.api.entities.Participant;
import lt.ktu.jonmic.api.entities.Tournament;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ParticipantService {

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private TournamentRepository tournamentRepository;

    @Autowired
    private TournamentService tournamentService;

    @Transactional
    public Participant save(Participant participant) {
        if (!participant.isNew()) {
            List<Tournament> tournaments = tournamentRepository.findByParticipantsId(participant.getId());
            for (Tournament tournament : tournaments) {
                tournamentService.save(tournament);
            }
        }

        return participantRepository.save(participant);
    }

    @Transactional
    public void delete(Participant participant) {
        participant.setDeleted(true);
        participantRepository.save(participant);
    }

}
