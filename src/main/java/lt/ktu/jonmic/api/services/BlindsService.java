package lt.ktu.jonmic.api.services;

import lt.ktu.jonmic.api.dao.BlindsRepository;
import lt.ktu.jonmic.api.dao.TournamentRepository;
import lt.ktu.jonmic.api.entities.Blinds;
import lt.ktu.jonmic.api.entities.Tournament;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BlindsService {

    @Autowired
    private BlindsRepository blindsRepository;

    @Autowired
    private TournamentRepository tournamentRepository;

    @Autowired
    private TournamentService tournamentService;

    @Transactional
    public Blinds save(Blinds blinds) {
        if (!blinds.isNew()) {
            List<Tournament> tournaments = tournamentRepository.findByBlindsId(blinds.getId());
            for (Tournament tournament : tournaments) {
                tournamentService.save(tournament);
            }
        }

        return blindsRepository.save(blinds);
    }

    @Transactional
    public void delete(Blinds blinds) {
        blinds.setDeleted(true);
        blindsRepository.save(blinds);
    }

}
