package lt.ktu.jonmic.api.services;

import lt.ktu.jonmic.api.dao.UserRepository;
import lt.ktu.jonmic.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Transactional
    public User save(User user) {
        if (user.getPassword() != null) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        } else {
            User existingUser = userRepository.findOne(user.getId());
            user.setPassword(existingUser.getPassword());
        }

        return userRepository.save(user);
    }

}
