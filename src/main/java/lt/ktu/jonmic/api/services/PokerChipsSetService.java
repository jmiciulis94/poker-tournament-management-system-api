package lt.ktu.jonmic.api.services;

import lt.ktu.jonmic.api.dao.PokerChipsSetRepository;
import lt.ktu.jonmic.api.dao.TournamentRepository;
import lt.ktu.jonmic.api.entities.PokerChipsSet;
import lt.ktu.jonmic.api.entities.Tournament;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PokerChipsSetService {

    @Autowired
    private PokerChipsSetRepository pokerChipsSetRepository;

    @Autowired
    private TournamentRepository tournamentRepository;

    @Autowired
    private TournamentService tournamentService;

    @Transactional
    public PokerChipsSet save(PokerChipsSet pokerChipsSet) {
        if (!pokerChipsSet.isNew()) {
            List<Tournament> tournaments = tournamentRepository.findByPokerChipsSetId(pokerChipsSet.getId());
            for (Tournament tournament : tournaments) {
                tournamentService.save(tournament);
            }
        }

        return pokerChipsSetRepository.save(pokerChipsSet);
    }

    @Transactional
    public void delete(PokerChipsSet pokerChipsSet) {
        pokerChipsSet.setDeleted(true);
        pokerChipsSetRepository.save(pokerChipsSet);
    }

}
