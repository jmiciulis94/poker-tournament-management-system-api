package lt.ktu.jonmic.api.services;

import lt.ktu.jonmic.api.dao.PauseRepository;
import lt.ktu.jonmic.api.dao.TournamentRepository;
import lt.ktu.jonmic.api.entities.Pause;
import lt.ktu.jonmic.api.entities.Tournament;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PauseService {

    @Autowired
    private PauseRepository pauseRepository;

    @Autowired
    private TournamentRepository tournamentRepository;

    @Autowired
    private TournamentService tournamentService;

    @Transactional
    public Pause save(Pause pause) {
        if (!pause.isNew()) {
            List<Tournament> tournaments = tournamentRepository.findByPauseId(pause.getId());
            for (Tournament tournament : tournaments) {
                tournamentService.save(tournament);
            }
        }

        return pauseRepository.save(pause);
    }

    @Transactional
    public void delete(Pause pause) {
        pause.setDeleted(true);
        pauseRepository.save(pause);
    }

}
