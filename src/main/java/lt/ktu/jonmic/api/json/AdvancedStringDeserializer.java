package lt.ktu.jonmic.api.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import java.io.IOException;

public class AdvancedStringDeserializer extends JsonDeserializer<String> {

    private StringDeserializer stringDeserializer = new StringDeserializer();

    @Override
    public String deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String result = stringDeserializer.deserialize(jsonParser, deserializationContext).trim();
        return result.length() > 0 ? result : null;
    }

}
