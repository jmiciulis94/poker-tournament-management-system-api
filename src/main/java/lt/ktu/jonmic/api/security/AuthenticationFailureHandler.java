package lt.ktu.jonmic.api.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lt.ktu.jonmic.api.exceptions.ServerError;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException ex) throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType("application/json");

        ServerError serverError = new ServerError(ex.getMessage(), new ArrayList<>());

        PrintWriter writer = response.getWriter();
        objectMapper.writeValue(writer, serverError);
        writer.flush();
    }

}
