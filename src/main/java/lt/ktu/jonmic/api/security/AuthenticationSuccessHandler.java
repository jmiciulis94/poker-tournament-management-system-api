package lt.ktu.jonmic.api.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lt.ktu.jonmic.api.entities.User;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("application/json");

        Principal principal = (Principal) authentication.getPrincipal();
        User user = principal.getUser();
        user.setPassword(null);

        PrintWriter writer = response.getWriter();
        objectMapper.writeValue(writer, user);
        writer.flush();
    }

}
