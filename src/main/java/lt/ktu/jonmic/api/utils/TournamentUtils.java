package lt.ktu.jonmic.api.utils;

import lt.ktu.jonmic.api.constants.Action;
import lt.ktu.jonmic.api.entities.Participant;
import lt.ktu.jonmic.api.entities.Tournament;
import lt.ktu.jonmic.api.entities.TournamentAction;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class TournamentUtils {

    public static Date getTournamentStartDate(Tournament tournament) {
        return tournament.getTournamentActions().stream().filter(ta -> ta.getAction() == Action.START).map(ta -> ta.getDate()).findFirst().orElse(null);
    }

    public static int getCurrentTournamentLevel(Tournament tournament) {
        int level = 1;
        for (TournamentAction tournamentAction : tournament.getTournamentActions()) {
            if (tournamentAction.getAction() == Action.LEVEL_UP || tournamentAction.getAction() == Action.RESTART_FROM_LEVEL) {
                level = Integer.valueOf(tournamentAction.getDetails());
            }
        }

        return level;
    }

    public static boolean isParticipantStillPlaying(Tournament tournament, Participant participant) {
        return tournament.getTournamentActions().stream().noneMatch(ta -> ta.getAction() == Action.PARTICIPANT_DROP && ta.getDetails().equals(String.valueOf(participant.getId())));
    }

    public static TournamentAction getLastTournamentAction(Tournament tournament, Action... importantActions) {
        List<TournamentAction> importantTournamentActions = tournament.getTournamentActions().stream().filter(ta -> Arrays.asList(importantActions).contains(ta.getAction())).collect(toList());
        return !importantTournamentActions.isEmpty() ? importantTournamentActions.get(importantTournamentActions.size() - 1) : null;
    }

    public static List<TournamentAction> getTournamentActionsAfterLastRestart(Tournament tournament) {
        Integer position = null;
        for (int i = 0; i < tournament.getTournamentActions().size(); i++) {
            if (tournament.getTournamentActions().get(i).getAction() == Action.RESTART_FROM_LEVEL) {
                position = i;
            }
        }

        if (position != null) {
            return tournament.getTournamentActions().subList(position, tournament.getTournamentActions().size());
        } else {
            return tournament.getTournamentActions();
        }
    }

    public static List<TournamentAction> getTournamentActionsAfterLevel(Tournament tournament, int level) {
        Integer position = null;
        for (int i = 0; i < tournament.getTournamentActions().size(); i++) {
            if (tournament.getTournamentActions().get(i).getAction() == Action.LEVEL_UP) {
                position = i;
            }
        }

        if (position != null) {
            return tournament.getTournamentActions().subList(position, tournament.getTournamentActions().size());
        } else {
            return tournament.getTournamentActions();
        }
    }

    public static boolean isTournamentPaused(Tournament tournament) {
        TournamentAction tournamentAction = getLastTournamentAction(tournament, Action.START, Action.FINISH, Action.PAUSE, Action.RESUME, Action.RESTART_FROM_LEVEL);
        if (tournamentAction != null) {
            return tournamentAction.getAction() == Action.PAUSE;
        }

        return false;
    }

    public static int countSecondsOnPause(Tournament tournament, Date now) {
        int secondsOnPause = 0;

        List<TournamentAction> tournamentActionsAfterRestart = getTournamentActionsAfterLastRestart(tournament);
        List<TournamentAction> importantActions = tournamentActionsAfterRestart.stream().filter(ta -> ta.getAction() == Action.PAUSE || ta.getAction() == Action.RESUME).collect(Collectors.toList());

        TournamentAction lastAction = null;
        for (TournamentAction tournamentAction : importantActions) {
            if (tournamentAction.getAction() == Action.PAUSE) {
                lastAction = tournamentAction;
            } else if (tournamentAction.getAction() == Action.RESUME) {
                secondsOnPause = secondsOnPause + DateUtils.getSecondsBetween(lastAction.getDate(), tournamentAction.getDate());
                lastAction = null;
            }
        }

        if (lastAction != null) {
            secondsOnPause = secondsOnPause + DateUtils.getSecondsBetween(lastAction.getDate(), now);
        }

        return secondsOnPause;
    }

    public static Date getNextLevelStartTime(Tournament tournament, int currentLevel, Date now) {
        int skippedLevels = 0;
        Date tournamentStartOrRestartDate = TournamentUtils.getTournamentStartDate(tournament);

        TournamentAction lastRestartFromLevelAction = getLastTournamentAction(tournament, Action.RESTART_FROM_LEVEL);
        if (lastRestartFromLevelAction != null) {
            skippedLevels = Integer.parseInt(lastRestartFromLevelAction.getDetails()) - 1;
            tournamentStartOrRestartDate = lastRestartFromLevelAction.getDate();
        }

        return DateUtils.addSeconds(DateUtils.addMinutes(tournamentStartOrRestartDate, (currentLevel - skippedLevels) * tournament.getLevelTime()), countSecondsOnPause(tournament, now));
    }

    public static boolean shouldTournamentBePaused(Tournament tournament, Date now) {
        if (tournament.getPause().getPauseMinute() != null) {
            return shouldHourlyTournamentBePaused(tournament, now);
        } else if (tournament.getPause().getNumberOfLevels() != null) {
            return shouldLevelTournamentBePaused(tournament, now);
        }

        return false;
    }

    private static boolean shouldHourlyTournamentBePaused(Tournament tournament, Date now) {
        Date pauseStarts = DateUtils.addMinutes(DateUtils.getStartOfHour(now), tournament.getPause().getPauseMinute());
        Date pauseEnds = DateUtils.addMinutes(DateUtils.getStartOfHour(now), tournament.getPause().getPauseMinute() + tournament.getPause().getPauseLength());
        if (pauseStarts.before(now) && pauseEnds.after(now)) {
            return true;
        }

        if (tournament.getPause().getPauseMinute() + tournament.getPause().getPauseLength() > 60) {
            Date lastHourPauseStarts = DateUtils.addMinutes(DateUtils.getStartOfHour(now), tournament.getPause().getPauseMinute() - 60);
            Date lastHourPauseEnds = DateUtils.addMinutes(DateUtils.getStartOfHour(now), tournament.getPause().getPauseMinute() + tournament.getPause().getPauseLength() - 60);
            if (lastHourPauseStarts.before(now) && lastHourPauseEnds.after(now)) {
                return true;
            }
        }

        return false;
    }

    private static boolean shouldLevelTournamentBePaused(Tournament tournament, Date now) {
        int currentLevel = getCurrentTournamentLevel(tournament);

        if (currentLevel == 1) {
            return false;
        }

        if ((currentLevel - 1) % tournament.getPause().getNumberOfLevels() == 0) {
            List<TournamentAction> tournamentActionsAfterCurrentLevel = getTournamentActionsAfterLevel(tournament, currentLevel);
            TournamentAction pauseAction = tournamentActionsAfterCurrentLevel.stream().filter(ta -> ta.getAction() == Action.PAUSE && ta.getUser() == null).findFirst().orElse(null);
            if (pauseAction == null) {
                return true;
            } else {
                Date pauseEnds = DateUtils.addMinutes(pauseAction.getDate(), tournament.getPause().getPauseLength());
                if (pauseEnds.after(now)) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        return false;
    }

}
