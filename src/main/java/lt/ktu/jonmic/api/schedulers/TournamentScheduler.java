package lt.ktu.jonmic.api.schedulers;

import lt.ktu.jonmic.api.dao.TournamentRepository;
import lt.ktu.jonmic.api.entities.Tournament;
import lt.ktu.jonmic.api.services.TournamentService;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TournamentScheduler {

    @Autowired
    private TournamentRepository tournamentRepository;

    @Autowired
    private TournamentService tournamentService;

    @Scheduled(fixedDelay = 1000)
    public void manageTournaments() {
        Date now = new Date();
        List<Tournament> tournaments = tournamentRepository.findOngoingTournaments();
        for (Tournament tournament : tournaments) {
            tournamentService.levelUpTournamentIfNeeded(tournament, now);
            tournamentService.pauseTournamentIfNeeded(tournament, now);
            tournamentService.resumeTournamentIfNeeded(tournament, now);
            tournamentService.stopTournamentIfItIsExpired(tournament, now);
            tournamentService.stopTournamentIfAllParticipantsAreDropped(tournament);
        }
    }

}
