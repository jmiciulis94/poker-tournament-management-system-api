package lt.ktu.jonmic.api.controllers;

import lt.ktu.jonmic.api.dao.ParticipantRepository;
import lt.ktu.jonmic.api.entities.Participant;
import lt.ktu.jonmic.api.services.ParticipantService;
import lt.ktu.jonmic.api.validators.ParticipantValidator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/participant")
public class ParticipantController extends AbstractController<Participant> {

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private ParticipantService participantService;

    @Autowired
    private ParticipantValidator participantValidator;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public Participant findById(@PathVariable(value = "id") Long id) {
        Participant participant = participantRepository.findOne(id);
        checkGetByIdSecurity(participant, "Participant");
        return participant;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<Participant> findAll() {
        return participantRepository.findByUserIdAndDeletedOrderByFirstNameAscLastNameAscNicknameAsc(securityService.getCurrentUser().getId(), false);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public Participant put(@RequestBody Participant participant) {
        checkPutSecurity(participant, participantRepository);
        validate(participant, "participant", participantValidator);

        participant.setUserId(securityService.getCurrentUser().getId());
        participant.setDeleted(false);

        return participantService.save(participant);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        Participant participant = participantRepository.findOne(id);
        checkDeleteSecurity(participant, participantRepository);
        participantService.delete(participant);
    }

}
