package lt.ktu.jonmic.api.controllers;

import lt.ktu.jonmic.api.constants.Action;
import lt.ktu.jonmic.api.dao.TournamentRepository;
import lt.ktu.jonmic.api.entities.Tournament;
import lt.ktu.jonmic.api.entities.User;
import lt.ktu.jonmic.api.exceptions.ValidationException;
import lt.ktu.jonmic.api.services.TournamentService;
import lt.ktu.jonmic.api.validators.TournamentValidator;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/tournament")
public class TournamentController extends AbstractController<Tournament> {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private TournamentRepository tournamentRepository;

    @Autowired
    private TournamentService tournamentService;

    @Autowired
    private TournamentValidator tournamentValidator;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public Tournament findById(@PathVariable(value = "id") Long id, @RequestParam(value = "password", required = false) String password, @RequestParam(value = "version", required = false) Integer version) {
        Tournament tournament = tournamentRepository.findOne(id);
        checkGetTournamentByIdSecurity(tournament, password);
        if (version == null || !tournament.getVersion().equals(version)) {
            return tournament;
        } else {
            return null;
        }
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<Tournament> findAll() {
        return tournamentRepository.findByUserIdAndDeletedOrderByIdDesc(securityService.getCurrentUser().getId(), false);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public Tournament put(@RequestBody Tournament tournament) {
        checkPutSecurity(tournament, tournamentRepository);
        validate(tournament, "tournament", tournamentValidator);

        tournament.setUserId(securityService.getCurrentUser().getId());
        tournament.setDeleted(false);

        if (!tournament.isNew()) {
            Tournament existingTournament = tournamentRepository.findOne(tournament.getId());
            tournament.setTournamentActions(existingTournament.getTournamentActions());
        }

        return tournamentService.save(tournament);
    }

    @RequestMapping(value = "/{id}/start", method = RequestMethod.POST)
    public void start(@PathVariable(value = "id") Long id) {
        Tournament tournament = tournamentRepository.findOne(id);
        checkGetByIdSecurity(tournament, "Tournament");
        validateStartAction(tournament);
        tournamentService.saveTournamentAction(tournament, Action.START, null, securityService.getCurrentUser());
    }

    @RequestMapping(value = "/{id}/finish", method = RequestMethod.POST)
    public void validateFinishAction(@PathVariable(value = "id") Long id) {
        Tournament tournament = tournamentRepository.findOne(id);
        checkGetByIdSecurity(tournament, "Tournament");
        validateFinishAction(tournament);
        tournamentService.saveTournamentAction(tournament, Action.FINISH, null, securityService.getCurrentUser());
    }

    @RequestMapping(value = "/{id}/pause", method = RequestMethod.POST)
    public void pause(@PathVariable(value = "id") Long id) {
        Tournament tournament = tournamentRepository.findOne(id);
        checkGetByIdSecurity(tournament, "Tournament");
        validatePauseAction(tournament);
        tournamentService.saveTournamentAction(tournament, Action.PAUSE, null, securityService.getCurrentUser());
    }

    @RequestMapping(value = "/{id}/resume", method = RequestMethod.POST)
    public void resume(@PathVariable(value = "id") Long id) {
        Tournament tournament = tournamentRepository.findOne(id);
        checkGetByIdSecurity(tournament, "Tournament");
        validateResumeAction(tournament);
        tournamentService.saveTournamentAction(tournament, Action.RESUME, null, securityService.getCurrentUser());
    }

    @RequestMapping(value = "/{id}/restart-from-level", method = RequestMethod.POST)
    public void restartFromLevel(@PathVariable(value = "id") Long id, @RequestBody Integer level) {
        Tournament tournament = tournamentRepository.findOne(id);
        checkGetByIdSecurity(tournament, "Tournament");
        validateRestartFromLevelAction(tournament, level);
        tournamentService.saveTournamentAction(tournament, Action.RESTART_FROM_LEVEL, String.valueOf(level), securityService.getCurrentUser());
    }

    @RequestMapping(value = "/{id}/participant/drop", method = RequestMethod.POST)
    public void participantDrop(@PathVariable(value = "id") Long id, @RequestBody Long participantId) {
        Tournament tournament = tournamentRepository.findOne(id);
        checkGetByIdSecurity(tournament, "Tournament");
        validateParticipantDropAction(tournament, participantId);
        tournamentService.saveTournamentAction(tournament, Action.PARTICIPANT_DROP, String.valueOf(participantId), securityService.getCurrentUser());
    }

    @RequestMapping(value = "/{id}/participant/re-buy", method = RequestMethod.POST)
    public void participantReBuy(@PathVariable(value = "id") Long id, @RequestBody Long participantId) {
        Tournament tournament = tournamentRepository.findOne(id);
        checkGetByIdSecurity(tournament, "Tournament");
        validateParticipantReBuyAction(tournament, participantId);
        tournamentService.saveTournamentAction(tournament, Action.PARTICIPANT_REBUY, String.valueOf(participantId), securityService.getCurrentUser());
    }

    @RequestMapping(value = "/{id}/participant/add-on", method = RequestMethod.POST)
    public void participantAddOn(@PathVariable(value = "id") Long id, @RequestBody Long participantId) {
        Tournament tournament = tournamentRepository.findOne(id);
        checkGetByIdSecurity(tournament, "Tournament");
        validateParticipantAddOnAction(tournament, participantId);
        tournamentService.saveTournamentAction(tournament, Action.PARTICIPANT_ADDON, String.valueOf(participantId), securityService.getCurrentUser());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        Tournament tournament = tournamentRepository.findOne(id);
        checkDeleteSecurity(tournament, tournamentRepository);
        tournamentService.delete(tournament);
    }

    protected void checkGetTournamentByIdSecurity(Tournament tournament, String password) {
        if (tournament == null || tournament.getDeleted()) {
            String message = messageSource.getMessage("NotExist.Tournament", null, Locale.getDefault());
            throw new IllegalArgumentException(message);
        }

        User currentUser = securityService.getCurrentUser();
        if (currentUser != null && tournament.getUserId().equals(currentUser.getId()) && password == null) {
            return;
        }

        if ((tournament.getPassword() == null && password != null) || (tournament.getPassword() != null && !tournament.getPassword().equals(password))) {
            String message = messageSource.getMessage("IncorrectPassword.tournament", null, Locale.getDefault());
            throw new IllegalArgumentException(message);
        }
    }

    private void validateStartAction(Tournament tournament) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(tournament, "tournament");
        tournamentValidator.validateStart(tournament, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }

    private void validateFinishAction(Tournament tournament) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(tournament, "tournament");
        tournamentValidator.validateFinish(tournament, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }

    private void validatePauseAction(Tournament tournament) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(tournament, "tournament");
        tournamentValidator.validatePause(tournament, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }

    private void validateResumeAction(Tournament tournament) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(tournament, "tournament");
        tournamentValidator.validateResume(tournament, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }

    private void validateRestartFromLevelAction(Tournament tournament, Integer level) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(tournament, "tournament");
        tournamentValidator.validateRestartFromLevel(tournament, level, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }

    private void validateParticipantDropAction(Tournament tournament, Long participantId) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(tournament, "tournament");
        tournamentValidator.validateParticipantDrop(tournament, participantId, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }

    private void validateParticipantReBuyAction(Tournament tournament, Long participantId) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(tournament, "tournament");
        tournamentValidator.validateParticipantReBuy(tournament, participantId, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }

    private void validateParticipantAddOnAction(Tournament tournament, Long participantId) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(tournament, "tournament");
        tournamentValidator.validateParticipantAddOn(tournament, participantId, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }

}
