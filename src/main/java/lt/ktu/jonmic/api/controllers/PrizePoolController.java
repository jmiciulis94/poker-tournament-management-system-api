package lt.ktu.jonmic.api.controllers;

import lt.ktu.jonmic.api.dao.PrizePoolRepository;
import lt.ktu.jonmic.api.entities.PrizePool;
import lt.ktu.jonmic.api.services.PrizePoolService;
import lt.ktu.jonmic.api.validators.PrizePoolValidator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/prize-pool")
public class PrizePoolController extends AbstractController<PrizePool> {

    @Autowired
    private PrizePoolRepository prizePoolRepository;

    @Autowired
    private PrizePoolService prizePoolService;

    @Autowired
    private PrizePoolValidator prizePoolValidator;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public PrizePool findById(@PathVariable(value = "id") Long id) {
        PrizePool prizePool = prizePoolRepository.findOne(id);
        checkGetByIdSecurity(prizePool, "PrizePool");
        return prizePool;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<PrizePool> findAll() {
        return prizePoolRepository.findByUserIdAndDeletedOrderByCaptionAsc(securityService.getCurrentUser().getId(), false);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public PrizePool put(@RequestBody PrizePool prizePool) {
        checkPutSecurity(prizePool, prizePoolRepository);
        validate(prizePool, "prizePool", prizePoolValidator);

        prizePool.setUserId(securityService.getCurrentUser().getId());
        prizePool.setDeleted(false);

        return prizePoolService.save(prizePool);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        PrizePool prizePool = prizePoolRepository.findOne(id);
        checkDeleteSecurity(prizePool, prizePoolRepository);
        prizePoolService.delete(prizePool);
    }

}
