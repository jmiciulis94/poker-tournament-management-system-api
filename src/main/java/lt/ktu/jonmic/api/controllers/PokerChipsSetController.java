package lt.ktu.jonmic.api.controllers;

import lt.ktu.jonmic.api.dao.PokerChipsSetRepository;
import lt.ktu.jonmic.api.entities.PokerChipsSet;
import lt.ktu.jonmic.api.services.PokerChipsSetService;
import lt.ktu.jonmic.api.validators.PokerChipsSetValidator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/poker-chips-set")
public class PokerChipsSetController extends AbstractController<PokerChipsSet> {

    @Autowired
    private PokerChipsSetRepository pokerChipsSetRepository;

    @Autowired
    private PokerChipsSetService pokerChipsSetService;

    @Autowired
    private PokerChipsSetValidator pokerChipsSetValidator;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public PokerChipsSet findById(@PathVariable(value = "id") Long id) {
        PokerChipsSet pokerChipsSet = pokerChipsSetRepository.findOne(id);
        checkGetByIdSecurity(pokerChipsSet, "PokerChipsSet");
        return pokerChipsSet;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<PokerChipsSet> findAll() {
        return pokerChipsSetRepository.findByUserIdAndDeletedOrderByCaptionAsc(securityService.getCurrentUser().getId(), false);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public PokerChipsSet put(@RequestBody PokerChipsSet pokerChipsSet) {
        checkPutSecurity(pokerChipsSet, pokerChipsSetRepository);
        validate(pokerChipsSet, "pokerChipsSet", pokerChipsSetValidator);

        pokerChipsSet.setUserId(securityService.getCurrentUser().getId());
        pokerChipsSet.setDeleted(false);

        return pokerChipsSetService.save(pokerChipsSet);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        PokerChipsSet pokerChipsSet = pokerChipsSetRepository.findOne(id);
        checkDeleteSecurity(pokerChipsSet, pokerChipsSetRepository);
        pokerChipsSetService.delete(pokerChipsSet);
    }

}
