package lt.ktu.jonmic.api.controllers;

import lt.ktu.jonmic.api.dao.UserRepository;
import lt.ktu.jonmic.api.entities.User;
import lt.ktu.jonmic.api.exceptions.ValidationException;
import lt.ktu.jonmic.api.security.SecurityService;
import lt.ktu.jonmic.api.services.UserService;
import lt.ktu.jonmic.api.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;

    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public Boolean ping() {
        return true;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public User findById() {
        return userRepository.findOne(securityService.getCurrentUser().getId());
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public User put(@RequestBody User user) {
        checkSecurity(user);
        validate(user);

        user = userService.save(user);
        user.setPassword(null);

        return user;
    }

    private void checkSecurity(User user) {
        User currentUser = securityService.getCurrentUser();
        if (!user.isNew() && !currentUser.equals(user)) {
            throw new SecurityException();
        }
    }

    private void validate(User user) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(user, "user");
        userValidator.validate(user, bindingResult);

        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }

}
