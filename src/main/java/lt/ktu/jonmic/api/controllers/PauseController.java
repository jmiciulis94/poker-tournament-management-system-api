package lt.ktu.jonmic.api.controllers;

import lt.ktu.jonmic.api.dao.PauseRepository;
import lt.ktu.jonmic.api.entities.Pause;
import lt.ktu.jonmic.api.services.PauseService;
import lt.ktu.jonmic.api.validators.PauseValidator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/pause")
public class PauseController extends AbstractController<Pause> {

    @Autowired
    private PauseRepository pauseRepository;

    @Autowired
    private PauseService pauseService;

    @Autowired
    private PauseValidator pauseValidator;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public Pause findById(@PathVariable(value = "id") Long id) {
        Pause pause = pauseRepository.findOne(id);
        checkGetByIdSecurity(pause, "Pause");
        return pause;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<Pause> findAll() {
        return pauseRepository.findByUserIdAndDeletedOrderByCaptionAsc(securityService.getCurrentUser().getId(), false);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public Pause put(@RequestBody Pause pause) {
        checkPutSecurity(pause, pauseRepository);
        validate(pause, "pause", pauseValidator);

        pause.setUserId(securityService.getCurrentUser().getId());
        pause.setDeleted(false);

        return pauseService.save(pause);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        Pause pause = pauseRepository.findOne(id);
        checkDeleteSecurity(pause, pauseRepository);
        pauseService.delete(pause);
    }

}
