package lt.ktu.jonmic.api.controllers;

import lt.ktu.jonmic.api.dao.AbstractRepository;
import lt.ktu.jonmic.api.entities.AbstractEntity;
import lt.ktu.jonmic.api.entities.Secured;
import lt.ktu.jonmic.api.exceptions.ValidationException;
import lt.ktu.jonmic.api.security.SecurityService;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Validator;

public abstract class AbstractController<T extends AbstractEntity & Secured> {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    protected SecurityService securityService;

    protected void checkGetByIdSecurity(T entity, String entityName) {
        if (entity == null) {
            String message = messageSource.getMessage("NotExist." + entityName, null, Locale.getDefault());
            throw new IllegalArgumentException(message);
        } else if (!entity.getUserId().equals(securityService.getCurrentUser().getId())) {
            throw new SecurityException();
        }
    }

    protected void checkPutSecurity(T entity, AbstractRepository<T> repository) {
        if (!entity.isNew()) {
            T existingEntity = repository.findOne(entity.getId());
            if (existingEntity == null) {
                throw new IllegalArgumentException();
            } else if (!existingEntity.getUserId().equals(securityService.getCurrentUser().getId())) {
                throw new SecurityException();
            }
        }
    }

    protected void checkDeleteSecurity(T entity, AbstractRepository<T> repository) {
        if (entity.isNew()) {
            throw new IllegalArgumentException();
        } else {
            T existingEntity = repository.findOne(entity.getId());
            if (!existingEntity.getUserId().equals(securityService.getCurrentUser().getId())) {
                throw new SecurityException();
            }
        }
    }

    protected void validate(T entity, String entityName, Validator validator) {
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(entity, entityName);
        validator.validate(entity, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
    }

}
