package lt.ktu.jonmic.api.controllers;

import lt.ktu.jonmic.api.dao.BlindsRepository;
import lt.ktu.jonmic.api.entities.Blinds;
import lt.ktu.jonmic.api.services.BlindsService;
import lt.ktu.jonmic.api.validators.BlindsValidator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/blinds")
public class BlindsController extends AbstractController<Blinds> {

    @Autowired
    private BlindsRepository blindsRepository;

    @Autowired
    private BlindsService blindsService;

    @Autowired
    private BlindsValidator blindsValidator;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public Blinds findById(@PathVariable(value = "id") Long id) {
        Blinds blinds = blindsRepository.findOne(id);
        checkGetByIdSecurity(blinds, "Blinds");
        return blinds;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<Blinds> findAll() {
        return blindsRepository.findByUserIdAndDeletedOrderByCaptionAsc(securityService.getCurrentUser().getId(), false);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public Blinds put(@RequestBody Blinds blinds) {
        checkPutSecurity(blinds, blindsRepository);
        validate(blinds, "blinds", blindsValidator);

        blinds.setUserId(securityService.getCurrentUser().getId());
        blinds.setDeleted(false);

        return blindsService.save(blinds);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        Blinds blinds = blindsRepository.findOne(id);
        checkDeleteSecurity(blinds, blindsRepository);
        blindsService.delete(blinds);
    }

}
