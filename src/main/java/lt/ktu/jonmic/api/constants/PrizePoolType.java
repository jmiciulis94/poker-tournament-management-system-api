package lt.ktu.jonmic.api.constants;

public enum PrizePoolType {

    CURRENCY, PERCENTAGES;

}
