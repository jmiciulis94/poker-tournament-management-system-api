package lt.ktu.jonmic.api.constants;

public enum Color {

    BLACK, BLUE, BROWN, GREEN, GREY, ORANGE, PINK, PURPLE, RED, WHITE, YELLOW;

}
