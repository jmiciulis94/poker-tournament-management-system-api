package lt.ktu.jonmic.api.dao;

import lt.ktu.jonmic.api.entities.Pause;

import java.util.List;

public interface PauseRepository extends AbstractRepository<Pause> {

    List<Pause> findByUserIdAndDeletedOrderByCaptionAsc(long userId, boolean deleted);

}
