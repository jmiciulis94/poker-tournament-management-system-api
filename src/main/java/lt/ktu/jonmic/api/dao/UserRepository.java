package lt.ktu.jonmic.api.dao;

import lt.ktu.jonmic.api.entities.User;

public interface UserRepository extends AbstractRepository<User> {

    User findByUsername(String username);

}
