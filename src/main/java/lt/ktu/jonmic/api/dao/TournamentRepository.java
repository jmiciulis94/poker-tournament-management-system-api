package lt.ktu.jonmic.api.dao;

import lt.ktu.jonmic.api.entities.Tournament;
import java.util.List;
import org.springframework.data.jpa.repository.Query;

public interface TournamentRepository extends AbstractRepository<Tournament> {

    List<Tournament> findByUserIdAndDeletedOrderByIdDesc(long userId, boolean deleted);

    List<Tournament> findByBlindsId(long blindsId);

    List<Tournament> findByPauseId(long pauseId);

    List<Tournament> findByPokerChipsSetId(long pokerChipsSetId);

    List<Tournament> findByPrizePoolId(long prizePoolId);

    List<Tournament> findByParticipantsId(long participantId);

    @Query("SELECT t FROM Tournament t WHERE EXISTS (SELECT 1 FROM Tournament t1 JOIN t1.tournamentActions ta1 WHERE t.id = t1.id AND ta1.action = 'START') AND NOT EXISTS (SELECT 1 FROM Tournament t1 JOIN t1.tournamentActions ta1 WHERE t.id = t1.id AND ta1.action = 'FINISH') AND t.deleted = false")
    List<Tournament> findOngoingTournaments();

}
