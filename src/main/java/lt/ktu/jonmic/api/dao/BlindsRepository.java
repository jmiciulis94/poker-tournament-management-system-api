package lt.ktu.jonmic.api.dao;

import lt.ktu.jonmic.api.entities.Blinds;
import java.util.List;

public interface BlindsRepository extends AbstractRepository<Blinds> {

    List<Blinds> findByUserIdAndDeletedOrderByCaptionAsc(long userId, boolean deleted);

}
