package lt.ktu.jonmic.api.dao;

import lt.ktu.jonmic.api.entities.PrizePool;

import java.util.List;

public interface PrizePoolRepository extends AbstractRepository<PrizePool> {

    List<PrizePool> findByUserIdAndDeletedOrderByCaptionAsc(long userId, boolean deleted);

}
