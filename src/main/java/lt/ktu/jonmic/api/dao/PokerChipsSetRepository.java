package lt.ktu.jonmic.api.dao;

import lt.ktu.jonmic.api.entities.PokerChipsSet;
import java.util.List;

public interface PokerChipsSetRepository extends AbstractRepository<PokerChipsSet> {

    List<PokerChipsSet> findByUserIdAndDeletedOrderByCaptionAsc(long userId, boolean deleted);

}
