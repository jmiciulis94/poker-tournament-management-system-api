package lt.ktu.jonmic.api.dao;

import lt.ktu.jonmic.api.entities.Participant;
import java.util.List;

public interface ParticipantRepository extends AbstractRepository<Participant> {

    List<Participant> findByUserIdAndDeletedOrderByFirstNameAscLastNameAscNicknameAsc(long userId, boolean deleted);

}
