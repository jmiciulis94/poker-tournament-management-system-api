package lt.ktu.jonmic.api.dao;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

@NoRepositoryBean
public interface AbstractRepository<T> extends Repository<T, Long> {

    T findOne(Long id);

    T save(T entity);

}
