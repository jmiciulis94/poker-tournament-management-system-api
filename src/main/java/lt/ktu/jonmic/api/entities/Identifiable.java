package lt.ktu.jonmic.api.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface Identifiable {

    Long getId();

    void setId(Long id);

    @JsonIgnore
    default boolean isNew() {
        return this.getId() == null;
    }

}
