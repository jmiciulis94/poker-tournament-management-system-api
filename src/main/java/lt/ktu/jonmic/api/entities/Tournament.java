package lt.ktu.jonmic.api.entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Getter
@Setter
@Entity
@Table(name = "tournament")
public class Tournament extends AbstractEntity implements Secured {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @SequenceGenerator(name = "tournamentIdGenerator", sequenceName = "tournament_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tournamentIdGenerator")
    private Long id;

    @Column(name = "caption", nullable = false, length = 500)
    @NotNull
    @Size(max = 500)
    private String caption;

    @Column(name = "password", length = 50)
    @Size(max = 50)
    private String password;

    @Column(name = "level_time", nullable = false)
    @NotNull
    @Min(1)
    private Integer levelTime;

    @Column(name = "buy_in")
    @Min(0)
    private BigDecimal buyIn;

    @Column(name = "re_buy")
    @Min(0)
    private BigDecimal reBuy;

    @Column(name = "add_on")
    @Min(0)
    private BigDecimal addOn;

    @Column(name = "add_on_level")
    @Min(2)
    private Integer addOnLevel;

    @Column(name = "chips_at_beginning")
    @Min(0)
    private BigDecimal chipsAtBeginning;

    @Column(name = "chips_for_re_buy")
    @Min(0)
    private BigDecimal chipsForReBuy;

    @Column(name = "chips_for_add_on")
    @Min(0)
    private BigDecimal chipsForAddOn;

    @JoinColumn(name = "blinds_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    @NotNull
    private Blinds blinds;

    @JoinColumn(name = "pause_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Pause pause;

    @JoinColumn(name = "poker_chips_set_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private PokerChipsSet pokerChipsSet;

    @JoinColumn(name = "prize_pool_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private PrizePool prizePool;

    @JoinTable(name = "tournament_participant", joinColumns = @JoinColumn(name = "tournament_id"), inverseJoinColumns = @JoinColumn(name = "participant_id"))
    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @OrderBy("firstName, lastName, nickname")
    @Valid
    private List<Participant> participants = new ArrayList<>();

    @JoinColumn(name = "tournament_id", referencedColumnName = "id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    @OrderBy(value = "date")
    @Valid
    private List<TournamentAction> tournamentActions = new ArrayList<>();

    @Column(name = "deleted", nullable = false)
    private Boolean deleted;

    @Column(name = "version", nullable = false)
    private Integer version;

    @Column(name = "user_id", nullable = false)
    private Long userId;

}
