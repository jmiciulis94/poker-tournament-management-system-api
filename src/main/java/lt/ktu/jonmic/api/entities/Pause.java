package lt.ktu.jonmic.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "pause")
public class Pause extends AbstractEntity implements Secured {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @SequenceGenerator(name = "pauseIdGenerator", sequenceName = "pause_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pauseIdGenerator")
    private Long id;

    @Column(name = "caption", nullable = false, length = 500)
    @NotNull
    @Size(max = 500)
    private String caption;

    @Column(name = "pause_minute")
    @Min(0)
    @Max(60)
    private Integer pauseMinute;

    @Column(name = "number_of_levels")
    @Min(1)
    private Integer numberOfLevels;

    @Column(name = "pause_length")
    @NotNull
    @Min(1)
    @Max(60)
    private Integer pauseLength;

    @Column(name = "add_on_pause_length")
    @Min(1)
    @Max(60)
    private Integer addOnPauseLength;

    @Column(name = "deleted", nullable = false)
    private Boolean deleted;

    @Column(name = "user_id", nullable = false)
    private Long userId;

}
