package lt.ktu.jonmic.api.entities;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "bet")
public class Bet extends AbstractEntity {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @SequenceGenerator(name = "betIdGenerator", sequenceName = "bet_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "betIdGenerator")
    private Long id;

    @Column(name = "level", nullable = false)
    @NotNull
    @Min(0)
    private Integer level;

    @Column(name = "big_blind", nullable = false)
    @NotNull
    @Min(0)
    private BigDecimal bigBlind;

    @Column(name = "small_blind", nullable = false)
    @NotNull
    @Min(0)
    private BigDecimal smallBlind;

    @Column(name = "ante")
    @Min(0)
    private BigDecimal ante;

}
