package lt.ktu.jonmic.api.entities;

import lt.ktu.jonmic.api.constants.Color;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "chip")
public class Chip extends AbstractEntity {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @SequenceGenerator(name = "chipIdGenerator", sequenceName = "chip_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "chipIdGenerator")
    private Long id;

    @Column(name = "color", nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    @NotNull
    private Color color;

    @Column(name = "value", nullable = false)
    @NotNull
    @Min(0)
    private BigDecimal value;

}
