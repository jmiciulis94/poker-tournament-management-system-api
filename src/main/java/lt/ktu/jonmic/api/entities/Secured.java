package lt.ktu.jonmic.api.entities;

public interface Secured {

    Long getUserId();

    void setUserId(Long userId);

}
