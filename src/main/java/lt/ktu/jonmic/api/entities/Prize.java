package lt.ktu.jonmic.api.entities;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "prize")
public class Prize extends AbstractEntity {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @SequenceGenerator(name = "prizeIdGenerator", sequenceName = "prize_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "prizeIdGenerator")
    private Long id;

    @Column(name = "place_from", nullable = false)
    @NotNull
    @Min(1)
    private Integer placeFrom;

    @Column(name = "place_until", nullable = false)
    @NotNull
    @Min(1)
    private Integer placeUntil;

    @Column(name = "amount", nullable = false)
    @NotNull
    @Min(0)
    private BigDecimal amount;

}
