package lt.ktu.jonmic.api.entities;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Getter
@Setter
@Entity
@Table(name = "blinds")
public class Blinds extends AbstractEntity implements Secured {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @SequenceGenerator(name = "blindsIdGenerator", sequenceName = "blinds_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "blindsIdGenerator")
    private Long id;

    @Column(name = "caption", nullable = false, length = 500)
    @NotNull
    @Size(max = 500)
    private String caption;

    @JoinColumn(name = "blinds_id", referencedColumnName = "id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    @OrderBy(value = "level")
    @Valid
    private List<Bet> bets = new ArrayList<>();

    @Column(name = "deleted", nullable = false)
    private Boolean deleted;

    @Column(name = "user_id", nullable = false)
    private Long userId;

}
