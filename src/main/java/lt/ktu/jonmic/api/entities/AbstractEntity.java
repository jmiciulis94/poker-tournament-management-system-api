package lt.ktu.jonmic.api.entities;

public abstract class AbstractEntity implements Identifiable {

    private static final int ODD_PRIME = 31;

    @Override
    public int hashCode() {
        return getId() == null ? 0 : ODD_PRIME * getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (getId() == null || obj == null || !(getClass().equals(obj.getClass()))) {
            return false;
        }

        return getId().equals(((Identifiable) obj).getId());
    }

}
