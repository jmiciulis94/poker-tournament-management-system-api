package lt.ktu.jonmic.api.validators;

import lt.ktu.jonmic.api.entities.Blinds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class BlindsValidator implements Validator {

    @Autowired
    private Validator validator;

    @Override
    public boolean supports(Class<?> clazz) {
        return Blinds.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        validator.validate(target, errors);
        Blinds blinds = (Blinds) target;

        if (blinds.getBets().isEmpty()) {
            errors.rejectValue("bets", "NotNull");
        }

        if (errors.hasErrors()) {
            errors.reject("Global");
        }
    }

}
