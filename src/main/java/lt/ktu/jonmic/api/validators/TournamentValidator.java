package lt.ktu.jonmic.api.validators;

import lt.ktu.jonmic.api.constants.Action;
import lt.ktu.jonmic.api.entities.Tournament;
import lt.ktu.jonmic.api.entities.TournamentAction;
import lt.ktu.jonmic.api.utils.TournamentUtils;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class TournamentValidator implements Validator {

    @Autowired
    private Validator validator;

    @Override
    public boolean supports(Class<?> clazz) {
        return Tournament.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        validator.validate(target, errors);
        Tournament tournament = (Tournament) target;

        if (tournament.getAddOn() == null && tournament.getAddOnLevel() != null) {
            errors.rejectValue("addOn", "NotNull");
        }

        if (tournament.getAddOnLevel() != null && tournament.getAddOnLevel().compareTo(tournament.getBlinds().getBets().size()) > 0) {
            errors.rejectValue("addOnLevel", "Custom");
        }

        if (errors.hasErrors()) {
            errors.reject("Global");
        }
    }

    public void validateStart(Tournament tournament, Errors errors) {
        validateThatActionDoesNotExists(tournament, Action.START, "AlreadyStarted", errors);
    }

    public void validateFinish(Tournament tournament, Errors errors) {
        validateThatActionExists(tournament, Action.START, "NotStarted", errors);
        validateThatActionDoesNotExists(tournament, Action.FINISH, "AlreadyFinished", errors);
    }

    public void validatePause(Tournament tournament, Errors errors) {
        validateThatActionExists(tournament, Action.START, "NotStarted", errors);
        validateThatActionDoesNotExists(tournament, Action.FINISH, "AlreadyFinished", errors);
        validateThatActionIsNotDuplicated(tournament, Action.PAUSE, Action.RESUME, "AlreadyPaused", errors);
    }

    public void validateResume(Tournament tournament, Errors errors) {
        validateThatActionExists(tournament, Action.START, "NotStarted", errors);
        validateThatActionDoesNotExists(tournament, Action.FINISH, "AlreadyFinished", errors);
        validateThatActionExists(tournament, Action.PAUSE, "AlreadyResumed", errors);
        validateThatActionIsNotDuplicated(tournament, Action.RESUME, Action.PAUSE, "AlreadyResumed", errors);
    }

    public void validateRestartFromLevel(Tournament tournament, Integer level, Errors errors) {
        validateThatActionExists(tournament, Action.START, "NotStarted", errors);
        validateThatActionDoesNotExists(tournament, Action.FINISH, "AlreadyFinished", errors);
        if (tournament.getBlinds().getBets().size() < level) {
            errors.reject("LevelTooBig");
        }
    }

    public void validateParticipantDrop(Tournament tournament, Long participantId, Errors errors) {
        validateThatActionExists(tournament, Action.START, "NotStarted", errors);
        validateThatActionDoesNotExists(tournament, Action.FINISH, "AlreadyFinished", errors);
        validateThatActionDoesNotExists(tournament, Action.PARTICIPANT_DROP, String.valueOf(participantId), "AlreadyDropped", errors);

        if (tournament.getParticipants().stream().noneMatch(p -> p.getId().equals(participantId))) {
            errors.reject("ParticipantDoesNotExist");
        }
    }

    public void validateParticipantReBuy(Tournament tournament, Long participantId, Errors errors) {
        validateThatActionExists(tournament, Action.START, "NotStarted", errors);
        validateThatActionDoesNotExists(tournament, Action.FINISH, "AlreadyFinished", errors);

        if (tournament.getParticipants().stream().noneMatch(p -> p.getId().equals(participantId))) {
            errors.reject("ParticipantDoesNotExist");
        }
    }

    public void validateParticipantAddOn(Tournament tournament, Long participantId, Errors errors) {
        validateThatActionExists(tournament, Action.START, "NotStarted", errors);
        validateThatActionDoesNotExists(tournament, Action.FINISH, "AlreadyFinished", errors);
        validateThatActionDoesNotExists(tournament, Action.PARTICIPANT_ADDON, String.valueOf(participantId), "AlreadyDidAddOn", errors);

        if (tournament.getParticipants().stream().noneMatch(p -> p.getId().equals(participantId))) {
            errors.reject("ParticipantDoesNotExist");
        }
    }

    private void validateThatActionIsNotDuplicated(Tournament tournament, Action action, Action unblockAction, String errorCode, Errors errors) {
        List<TournamentAction> tournamentActions = TournamentUtils.getTournamentActionsAfterLastRestart(tournament);

        List<TournamentAction> importantActions = tournamentActions.stream().filter(ta -> ta.getAction() == action || ta.getAction() == unblockAction).collect(Collectors.toList());
        if (!importantActions.isEmpty() && importantActions.get(importantActions.size() - 1).getAction() == action) {
            errors.reject(errorCode);
        }
    }

    private void validateThatActionExists(Tournament tournament, Action action, String errorCode, Errors errors) {
        if (tournament.getTournamentActions().stream().noneMatch(ta -> ta.getAction() == action)) {
            errors.reject(errorCode);
        }
    }

    private void validateThatActionDoesNotExists(Tournament tournament, Action action, String errorCode, Errors errors) {
        if (tournament.getTournamentActions().stream().anyMatch(ta -> ta.getAction() == action)) {
            errors.reject(errorCode);
        }
    }

    private void validateThatActionDoesNotExists(Tournament tournament, Action action, String actionDetails, String errorCode, Errors errors) {
        if (tournament.getTournamentActions().stream().anyMatch(ta -> ta.getAction() == action && ta.getDetails().equals(actionDetails))) {
            errors.reject(errorCode);
        }
    }

}
