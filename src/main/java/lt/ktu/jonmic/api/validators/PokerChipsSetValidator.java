package lt.ktu.jonmic.api.validators;

import lt.ktu.jonmic.api.constants.Color;
import lt.ktu.jonmic.api.entities.Chip;
import lt.ktu.jonmic.api.entities.PokerChipsSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class PokerChipsSetValidator implements Validator {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private Validator validator;

    @Override
    public boolean supports(Class<?> clazz) {
        return PokerChipsSet.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        validator.validate(target, errors);
        PokerChipsSet pokerChipsSet = (PokerChipsSet) target;

        if (pokerChipsSet.getChips().isEmpty()) {
            errors.rejectValue("chips", "NotNull");
        }

        List<Color> usedColors = new ArrayList<>();
        for (int i = 0; i < pokerChipsSet.getChips().size(); i++) {
            Chip chip = pokerChipsSet.getChips().get(i);
            if (usedColors.contains(chip.getColor())) {
                errors.rejectValue(String.format("chips[%d].color", i), "Custom", new String[]{messageSource.getMessage("color." + chip.getColor().name().toLowerCase(), null, Locale.getDefault())}, "");
            } else {
                usedColors.add(chip.getColor());
            }
        }

        if (errors.hasErrors()) {
            errors.reject("Global");
        }
    }

}
