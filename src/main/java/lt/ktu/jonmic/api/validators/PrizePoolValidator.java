package lt.ktu.jonmic.api.validators;

import lt.ktu.jonmic.api.comparators.PrizeComparator;
import lt.ktu.jonmic.api.constants.PrizePoolType;
import lt.ktu.jonmic.api.entities.Prize;
import lt.ktu.jonmic.api.entities.PrizePool;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class PrizePoolValidator implements Validator {

    @Autowired
    private Validator validator;

    @Override
    public boolean supports(Class<?> clazz) {
        return PrizePool.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        validator.validate(target, errors);
        PrizePool prizePool = (PrizePool) target;

        if (prizePool.getPrizes().isEmpty()) {
            errors.rejectValue("prizes", "NotNull");
        }

        for (int i = 0; i < prizePool.getPrizes().size(); i++) {
            Prize prize = prizePool.getPrizes().get(i);
            if (prize.getPlaceFrom() != null && prize.getPlaceUntil() != null && prize.getPlaceFrom() > prize.getPlaceUntil()) {
                errors.rejectValue(String.format("prizes[%d].placeFrom", i), "Custom");
            }
        }

        if (prizePool.getType() == PrizePoolType.PERCENTAGES) {
            BigDecimal sum = prizePool.getRake() != null ? prizePool.getRake() : BigDecimal.ZERO;
            for (Prize prize : prizePool.getPrizes()) {
                sum = sum.add(prize.getAmount().multiply(new BigDecimal(prize.getPlaceUntil() - prize.getPlaceFrom() + 1)));
            }

            if (sum.compareTo(new BigDecimal("100")) != 0) {
                errors.rejectValue("prizes", "Custom1");
            }
        } else if (prizePool.getType() == PrizePoolType.CURRENCY) {
            if (prizePool.getRake() != null) {
                errors.rejectValue("rake", "Custom");
            }
        }

        if (!errors.hasErrors()) {
            int lastUntil = 0;
            List<Prize> prizes = prizePool.getPrizes().stream().sorted(new PrizeComparator()).collect(Collectors.toList());
            for (Prize prize : prizes) {
                if (prize.getPlaceFrom() != lastUntil + 1) {
                    errors.rejectValue("prizes", "Custom2");
                }
                lastUntil = prize.getPlaceUntil();
            }
        }

        if (errors.hasErrors()) {
            errors.reject("Global");
        }
    }

}
