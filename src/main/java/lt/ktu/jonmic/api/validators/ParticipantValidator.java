package lt.ktu.jonmic.api.validators;

import lt.ktu.jonmic.api.entities.Participant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ParticipantValidator implements Validator {

    @Autowired
    private Validator validator;

    @Override
    public boolean supports(Class<?> clazz) {
        return Participant.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        validator.validate(target, errors);
        Participant participant = (Participant) target;

        if (participant.getFirstName() == null && participant.getLastName() == null && participant.getNickname() == null) {
            errors.rejectValue("firstName", "Custom");
        }

        if (errors.hasErrors()) {
            errors.reject("Global");
        }
    }

}
