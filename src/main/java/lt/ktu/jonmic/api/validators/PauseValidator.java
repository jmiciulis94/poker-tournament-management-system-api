package lt.ktu.jonmic.api.validators;

import lt.ktu.jonmic.api.entities.Pause;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class PauseValidator implements Validator {

    @Autowired
    private Validator validator;

    @Override
    public boolean supports(Class<?> clazz) {
        return Pause.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        validator.validate(target, errors);
        Pause pause = (Pause) target;

        if (pause.getPauseMinute() == null && pause.getNumberOfLevels() == null) {
            errors.rejectValue("pauseMinute", "Custom1");
        }

        if (pause.getPauseMinute() != null && pause.getNumberOfLevels() != null) {
            errors.rejectValue("pauseMinute", "Custom2");
        }

        if (errors.hasErrors()) {
            errors.reject("Global");
        }
    }

}
