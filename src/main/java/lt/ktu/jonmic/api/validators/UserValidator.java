package lt.ktu.jonmic.api.validators;

import lt.ktu.jonmic.api.dao.UserRepository;
import lt.ktu.jonmic.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private Validator validator;

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        validator.validate(target, errors);
        User user = (User) target;

        User existingUser = userRepository.findByUsername(user.getUsername());
        if (user.isNew()) {
            if (existingUser != null) {
                errors.rejectValue("username", "Unique");
            }

            if (user.getPassword() == null) {
                errors.rejectValue("password", "NotNull");
            }
        } else {
            if (existingUser != null && !existingUser.getId().equals(user.getId())) {
                errors.rejectValue("username", "Unique");
            }
        }

        if (errors.hasErrors()) {
            errors.reject("Global");
        }
    }

}
