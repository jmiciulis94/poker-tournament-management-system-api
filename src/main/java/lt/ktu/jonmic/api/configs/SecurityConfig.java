package lt.ktu.jonmic.api.configs;

import lt.ktu.jonmic.api.security.AuthenticationFailureHandler;
import lt.ktu.jonmic.api.security.AuthenticationSuccessHandler;
import lt.ktu.jonmic.api.security.HttpAuthenticationEntryPoint;
import lt.ktu.jonmic.api.security.HttpLogoutSuccessHandler;
import lt.ktu.jonmic.api.security.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthenticationFailureHandler authenticationFailureHandler;

    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private HttpAuthenticationEntryPoint httpAuthenticationEntryPoint;

    @Autowired
    private HttpLogoutSuccessHandler httpLogoutSuccessHandler;

    @Autowired
    private SecurityService securityService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(securityService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        authenticationManagerBuilder.authenticationProvider(authenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf().disable();
        httpSecurity.exceptionHandling().authenticationEntryPoint(httpAuthenticationEntryPoint);
        httpSecurity.formLogin().permitAll().loginProcessingUrl("/login").usernameParameter("username").passwordParameter("password").successHandler(authenticationSuccessHandler).failureHandler(authenticationFailureHandler);
        httpSecurity.logout().permitAll().logoutUrl("/logout").logoutSuccessHandler(httpLogoutSuccessHandler);
        httpSecurity.sessionManagement().maximumSessions(1);
        httpSecurity.authorizeRequests().antMatchers("/user").permitAll();
        httpSecurity.authorizeRequests().antMatchers(HttpMethod.GET, "/tournament/?*").permitAll();
        httpSecurity.authorizeRequests().anyRequest().authenticated();
    }

}
