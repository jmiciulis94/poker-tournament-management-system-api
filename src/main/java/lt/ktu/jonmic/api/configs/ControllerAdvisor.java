package lt.ktu.jonmic.api.configs;

import lt.ktu.jonmic.api.exceptions.ServerError;
import lt.ktu.jonmic.api.exceptions.ValidationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ControllerAdvisor {

    private static final Logger LOGGER = Logger.getLogger(ControllerAdvisor.class);

    @Autowired
    private MessageSource messageSource;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor("", true));
    }

    @ExceptionHandler(value = ValidationException.class)
    @ResponseBody
    public ServerError handleValidationException(HttpServletResponse response, ValidationException ex) {
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

        String header = messageSource.getMessage(ex.getErrors().getGlobalError(), Locale.getDefault());
        List<String> items = new ArrayList<>();
        for (FieldError fieldError : ex.getErrors().getFieldErrors()) {
            items.add(messageSource.getMessage(fieldError, Locale.getDefault()));
        }

        return new ServerError(header, items);
    }

    @ExceptionHandler(value = SecurityException.class)
    @ResponseBody
    public ServerError handleSecurityException(HttpServletResponse response) {
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        String header = messageSource.getMessage("unauthorizedResource", null, Locale.getDefault());
        return new ServerError(header, new ArrayList<>());
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    @ResponseBody
    public ServerError handleIllegalArgumentException(HttpServletResponse response, IllegalArgumentException ex) {
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        String header = ex.getMessage();
        return new ServerError(header, new ArrayList<>());
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ServerError handleGeneralException(HttpServletResponse response, Exception ex) {
        LOGGER.error(ex.getMessage(), ex);

        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        String header = messageSource.getMessage("serverError", null, Locale.getDefault());
        return new ServerError(header, new ArrayList<>());
    }

}
