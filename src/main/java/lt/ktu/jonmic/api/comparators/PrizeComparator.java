package lt.ktu.jonmic.api.comparators;

import lt.ktu.jonmic.api.entities.Prize;

import java.util.Comparator;

public class PrizeComparator implements Comparator<Prize> {

    @Override
    public int compare(Prize p1, Prize p2) {
        return p1.getPlaceFrom().compareTo(p2.getPlaceFrom());
    }

}
